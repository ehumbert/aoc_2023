package fr.ehumbert.aoc23

import fr.ehumbert.aoc23.utils.Coordinates
import fr.ehumbert.aoc23.utils.Grid
import fr.ehumbert.aoc23.utils.IoUtils

class Day17 : AbstractSolver() {

    private val grid = Grid(IoUtils.readAllInputLines("day17", false))

    override fun solvePart1(): Any {
        return moveCrucible(1, 3)
    }

    override fun solvePart2(): Any {
        return moveCrucible(4, 10)
    }

    private fun moveCrucible(minMove: Int, maxMove: Int): Int {

        val start = Coordinates(0, 0)
        val end = Coordinates(grid.maxX, grid.maxY)

        val toVisit: MutableSet<Triple<Int, Coordinates, Int>> = mutableSetOf(Triple(0, start, 0), Triple(0, start, 1))
        val seen: MutableSet<Pair<Coordinates, Int>> = mutableSetOf()

        while (toVisit.isNotEmpty()) {

            val current = toVisit.minBy { it.first }
            val (score, currentCoordinates, axis) = current

            if (currentCoordinates == end) {
                return score
            }

            toVisit.remove(current)

            if (Pair(currentCoordinates, axis) in seen) {
                continue
            }

            seen.add(Pair(currentCoordinates, axis))

            for (j in listOf(-1, 1)) {

                var tentativeScore = score

                for (i in 1..maxMove) {

                    val next = when (axis) {
                        0 -> Pair(currentCoordinates.first + (i * j), currentCoordinates.second)
                        else -> Pair(currentCoordinates.first, currentCoordinates.second + (i * j))
                    }

                    grid.getTile(next)?.let { nextTile ->

                        tentativeScore += nextTile.digitToInt()

                        if (i >= minMove) {

                            val turn = Triple(tentativeScore, next, 1 - axis)

                            if (Pair(turn.second, turn.third) !in seen) {
                                toVisit.add(turn)
                            }
                        }
                    }
                }
            }
        }

        return -1
    }

}

fun main() {
    Day17().solve()
}