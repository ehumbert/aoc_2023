package fr.ehumbert.aoc23

import fr.ehumbert.aoc23.utils.Coordinates3D
import fr.ehumbert.aoc23.utils.IoUtils

class Day22 : AbstractSolver() {

    private val bricks: MutableList<Brick> = mutableListOf()

    private val supportBricks: MutableMap<Int, MutableList<Int>> = mutableMapOf()
    private val supportedBricks: MutableMap<Int, MutableList<Int>> = mutableMapOf()

    private val removableBricks = mutableSetOf<Int>()

    override fun solvePart1(): Any {

        var i = 0

        IoUtils.forEachInputLines("day22", false) { line ->

            val (start, end) = line.split('~').map { coordinate -> coordinate.split(',').map { it.toInt() } }

            val brick = Brick(Coordinates3D(start[0], start[1], start[2]), Coordinates3D(end[0], end[1], end[2]), i)

            bricks.add(brick)
            supportBricks[brick.id] = mutableListOf()
            supportedBricks[brick.id] = mutableListOf()

            i++
        }

        bricks.sortBy { it.start.z }

        bricks.forEach { brickToMove ->

            if (brickToMove.start.z == 1) {
                return@forEach
            }

            val bricksBelow = bricks.filter { it.end.z < brickToMove.start.z }.sortedByDescending { it.end.z }
            var bottom = true

            val zDiff = brickToMove.end.z - brickToMove.start.z

            for (brick in bricksBelow) {
                if (doBricksCollide(brick, brickToMove)) {
                    brickToMove.start.z = brick.end.z + 1
                    brickToMove.end.z = brickToMove.start.z + zDiff
                    bottom = false
                    break
                }
            }

            if (bottom) {
                brickToMove.start.z = 1
                brickToMove.end.z = brickToMove.start.z + zDiff
            }
        }

        bricks.forEach { brick ->
            val others = (bricks - brick)
            others.forEach {
                if (doFirstBrickSupportSecond(brick, it)) {
                    supportBricks[it.id]!!.add(brick.id)
                    supportedBricks[brick.id]!!.add(it.id)
                }
            }
        }

        supportBricks.forEach { (uuid, supports) ->

            if (supports.size > 1) {

                supports.forEach { support ->

                    // check if the support is not mandatory for its other dependencies
                    if ((supportedBricks[support]!! - uuid).all { supportBricks[it]!!.size > 1 }) {
                        removableBricks.add(support)
                    }
                }
            }
        }

        removableBricks.addAll(supportedBricks.filter { it.value.size == 0 }.map { it.key })

        return removableBricks.size
    }

    override fun solvePart2(): Any {

        var total = 0

        bricks.filter { it.id !in removableBricks }.forEach { brick ->

            val queue = mutableListOf(brick.id)
            val falling = queue.toMutableSet()

            while (queue.isNotEmpty()) {

                val fallingBrick = queue.removeFirst()

                (supportedBricks[fallingBrick]!! - falling).forEach { supported ->

                    // A brick can fall if all of its supports are down
                    if (falling.containsAll(supportBricks[supported]!!)) {
                        queue.add(supported)
                        falling.add(supported)
                    }
                }
            }

            total += falling.size - 1 // -1 account for the starting brick
        }

        return total
    }

    private fun doBricksCollide(a: Brick, b: Brick): Boolean {

        val collideX = a.getXRange().intersect(b.getXRange()).isNotEmpty()
        val collideY = a.getYRange().intersect(b.getYRange()).isNotEmpty()

        return collideX && collideY
    }

    private fun doFirstBrickSupportSecond(a: Brick, b: Brick): Boolean {

        val bOnTopOfA = a.end.z + 1 == b.start.z

        return doBricksCollide(a, b) && bOnTopOfA
    }

}

data class Brick(val start: Coordinates3D, val end: Coordinates3D, val id: Int)

fun Brick.getXRange() = this.start.x..this.end.x
fun Brick.getYRange() = this.start.y..this.end.y

fun main() {
    Day22().solve()
}
