package fr.ehumbert.aoc23

import fr.ehumbert.aoc23.utils.Coordinates
import fr.ehumbert.aoc23.utils.Direction
import fr.ehumbert.aoc23.utils.Grid
import fr.ehumbert.aoc23.utils.IoUtils
import kotlin.math.max

class Day16 : AbstractSolver() {

    private val grid = Grid(IoUtils.readAllInputLines("day16"))
    private val visited = mutableSetOf<Pair<Coordinates, Direction>>()

    override fun solvePart1(): Any {

        moveBeam(Coordinates(0, 0), Direction.RIGHT)

        val visitedCoordinates = visited.map { it.first }.toSet()

        return visitedCoordinates.size
    }

    override fun solvePart2(): Any {

        var maxScore = 0

        // top edge
        for (x in 0..grid.maxX) {
            visited.clear()
            moveBeam(Coordinates(x, 0), Direction.DOWN)
            maxScore = max(maxScore, visited.map { it.first }.toSet().size)
        }

        // right edge
        for (y in 0..grid.maxY) {
            visited.clear()
            moveBeam(Coordinates(grid.maxX, y), Direction.LEFT)
            maxScore = max(maxScore, visited.map { it.first }.toSet().size)
        }

        // bottom edge
        for (x in 0..grid.maxX) {
            visited.clear()
            moveBeam(Coordinates(x, grid.maxY), Direction.UP)
            maxScore = max(maxScore, visited.map { it.first }.toSet().size)
        }

        // left edge
        for (y in 0..grid.maxY) {
            visited.clear()
            moveBeam(Coordinates(0, y), Direction.RIGHT)
            maxScore = max(maxScore, visited.map { it.first }.toSet().size)
        }

        return maxScore
    }

    private fun moveBeam(coordinates: Coordinates, direction: Direction) {

        val toProcess = mutableListOf(Pair(coordinates, direction))

        while (toProcess.isNotEmpty()) {

            val current = toProcess.removeFirst()
            val (currentCoordinates, currentDirection) = current

            val tile = grid.getTile(currentCoordinates)

            if (current in visited) {
                continue
            }

            tile?.let {
                visited.add(current)
            }

            when (tile) {

                '.' -> toProcess.add(Pair(grid.getNextInDirection(currentCoordinates, currentDirection), currentDirection))

                '|' -> when (currentDirection) {
                    Direction.UP, Direction.DOWN -> toProcess.add(Pair(grid.getNextInDirection(currentCoordinates, currentDirection), currentDirection))
                    Direction.LEFT, Direction.RIGHT -> {
                        toProcess.add(Pair(grid.getNextInDirection(currentCoordinates, Direction.UP), Direction.UP))
                        toProcess.add(Pair(grid.getNextInDirection(currentCoordinates, Direction.DOWN), Direction.DOWN))
                    }
                }

                '-' -> when (currentDirection) {
                    Direction.LEFT, Direction.RIGHT -> toProcess.add(Pair(grid.getNextInDirection(currentCoordinates, currentDirection), currentDirection))
                    Direction.UP, Direction.DOWN -> {
                        toProcess.add(Pair(grid.getNextInDirection(currentCoordinates, Direction.LEFT), Direction.LEFT))
                        toProcess.add(Pair(grid.getNextInDirection(currentCoordinates, Direction.RIGHT), Direction.RIGHT))
                    }
                }

                '\\' -> when (currentDirection) {
                    Direction.LEFT -> toProcess.add(Pair(grid.getNextInDirection(currentCoordinates, Direction.UP), Direction.UP))
                    Direction.RIGHT -> toProcess.add(Pair(grid.getNextInDirection(currentCoordinates, Direction.DOWN), Direction.DOWN))
                    Direction.UP -> toProcess.add(Pair(grid.getNextInDirection(currentCoordinates, Direction.LEFT), Direction.LEFT))
                    Direction.DOWN -> toProcess.add(Pair(grid.getNextInDirection(currentCoordinates, Direction.RIGHT), Direction.RIGHT))
                }

                '/' -> when (currentDirection) {
                    Direction.LEFT -> toProcess.add(Pair(grid.getNextInDirection(currentCoordinates, Direction.DOWN), Direction.DOWN))
                    Direction.RIGHT -> toProcess.add(Pair(grid.getNextInDirection(currentCoordinates, Direction.UP), Direction.UP))
                    Direction.UP -> toProcess.add(Pair(grid.getNextInDirection(currentCoordinates, Direction.RIGHT), Direction.RIGHT))
                    Direction.DOWN -> toProcess.add(Pair(grid.getNextInDirection(currentCoordinates, Direction.LEFT), Direction.LEFT))
                }
            }
        }
    }

}

fun main() {
    Day16().solve()
}