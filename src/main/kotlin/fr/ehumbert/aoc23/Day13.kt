package fr.ehumbert.aoc23

import fr.ehumbert.aoc23.utils.IoUtils
import kotlin.math.min

class Day13 : AbstractSolver() {

    override fun solvePart1(): Any {

        var result = 0

        val lines = mutableListOf<String>()
        val columns = mutableListOf<String>()

        fun score() {

            for (i in 0..<lines.first().length) {
                columns.add(lines.map { it[i] }.joinToString(""))
            }

            result += scoreHorizontalReflections(lines) + scoreVerticalReflections(columns)

            lines.clear()
            columns.clear()
        }

        IoUtils.forEachInputLines("day13") { line ->

            if (line.isBlank()) {
                score()
            } else {
                lines.add(line)
            }
        }

        score()

        return result
    }

    override fun solvePart2(): Any {

        var result = 0

        val lines = mutableListOf<String>()
        val columns = mutableListOf<String>()

        fun score() {

            for (i in 0..<lines.first().length) {
                columns.add(lines.map { it[i] }.joinToString(""))
            }

            var reflectionIndex = fixSmudge(lines)

            if (reflectionIndex >= 0) {
                result += scoreHorizontalReflections(lines, reflectionIndex)
            } else {
                reflectionIndex = fixSmudge(columns)
                result += scoreVerticalReflections(columns, reflectionIndex)
            }

            lines.clear()
            columns.clear()
        }

        IoUtils.forEachInputLines("day13") { line ->

            if (line.isBlank()) {
                score()
            } else {
                lines.add(line)
            }
        }

        score()

        return result
    }

    private fun doLinesReflectAtIndex(lines: List<String>, i: Int): Boolean {

        if (i == lines.size - 1) {
            return false
        }

        val len = min(i + 1, lines.size - (i + 1))

        val a = lines.slice(i + 1 - len..i)
        val b = lines.slice(i + 1..i + len).reversed()

        return a == b
    }

    private fun scoreHorizontalReflections(lines: List<String>, part2: Int = -1): Int {

        if (part2 >= 0) {
            return 100 * (part2 + 1)
        }

        var result = 0

        for (y in lines.indices) {
            if (doLinesReflectAtIndex(lines, y)) {
                result += 100 * (y + 1)
            }
        }

        return result
    }

    private fun scoreVerticalReflections(columns: List<String>, part2: Int = -1): Int {

        if (part2 >= 0) {
            return part2 + 1
        }

        var result = 0

        for (x in columns.indices) {
            if (doLinesReflectAtIndex(columns, x)) {
                result += x + 1
            }
        }

        return result
    }

    private fun getDifferencesIndex(a: String, b: String): List<Int> {

        val differences = mutableListOf<Int>()

        if (a.length != b.length) {
            throw RuntimeException("Can't compare strings of different sizes")
        }

        for (i in a.indices) {
            if (a[i] != b[i]) {
                differences.add(i)
            }
        }

        return differences
    }

    private fun fixSmudge(lines: MutableList<String>): Int {

        for (i in lines.indices) {

            if (i == lines.size - 1) {
                break
            }

            val len = min(i + 1, lines.size - (i + 1))

            val a = lines.slice(i - len + 1..i)
            val b = lines.slice(i + 1..i + len).reversed()

            val toCompare = a.zip(b)

            var diff = 0
            var smudge: Triple<Int, Int, Int>? = null

            for (j in toCompare.indices) {

                val (lineA, lineB) = toCompare[j]

                val lineDiff = getDifferencesIndex(lineA, lineB)
                diff += lineDiff.size

                if (diff > 1) {
                    smudge = null
                    break
                }

                if (lineDiff.size == 1) {
                    smudge = Triple(i, j, lineDiff.first())
                }
            }

            smudge?.let { (lineReflection, lineToChange, charToChange) ->

                val tmp = lines[lineToChange].toCharArray()
                tmp[charToChange] = if (tmp[charToChange] == '.') '#' else '.'

                val fixedLine = String(tmp)

                val buffer = lines.slice(0..<lineToChange) + listOf(fixedLine) + lines.slice(lineToChange + 1..<lines.size)
                lines.clear()
                lines.addAll(buffer)

                return lineReflection
            }
        }

        return -1
    }

}

fun main() {
    Day13().solve()
}