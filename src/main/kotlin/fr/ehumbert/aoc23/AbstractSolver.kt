package fr.ehumbert.aoc23

abstract class AbstractSolver {

    fun solve() {
        println("Part 1 : ${solvePart1()}")
        println("Part 2 : ${solvePart2()}")
    }

    abstract fun solvePart1(): Any

    abstract fun solvePart2(): Any

}