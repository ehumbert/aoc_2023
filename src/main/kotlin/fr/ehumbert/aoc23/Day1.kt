package fr.ehumbert.aoc23

import fr.ehumbert.aoc23.utils.IoUtils

class Day1 : AbstractSolver() {

    override fun solvePart1(): Any {

        var sum = 0

        IoUtils.forEachInputLines("day1") { line ->

            val firstDigit = line.first { it.isDigit() }
            val lastDigit = line.last { it.isDigit() }

            val key = "$firstDigit$lastDigit".toInt()

            sum += key
        }

        return sum
    }

    override fun solvePart2(): Any {

        var sum = 0
        val numbers = listOf("one", "two", "three", "four", "five", "six", "seven", "eight", "nine") + (1..9).map { it.toString() }

        IoUtils.forEachInputLines("day1") { line ->

            val firstDigit = parseStringDigit(line.findAnyOf(numbers)!!.second)
            val lastDigit = parseStringDigit(line.findLastAnyOf(numbers)!!.second)

            val key = "$firstDigit$lastDigit".toInt()

            sum += key
        }

        return sum
    }

    private fun parseStringDigit(digit: String): Int = when (digit) {
        "one" -> 1
        "two" -> 2
        "three" -> 3
        "four" -> 4
        "five" -> 5
        "six" -> 6
        "seven" -> 7
        "eight" -> 8
        "nine" -> 9
        else -> digit.toInt()
    }
}

fun main() {
    Day1().solve()
}