package fr.ehumbert.aoc23

import fr.ehumbert.aoc23.utils.IoUtils
import kotlin.math.pow

class Day4 : AbstractSolver() {

    private val cards: MutableMap<Int, Card> = mutableMapOf()

    override fun solvePart1(): Any {

        val numberRegex = Regex("\\d+")

        var score = 0.0

        IoUtils.forEachInputLines("day4") { line ->

            val input = line.substring(line.indexOf(':') + 1).split('|')

            val winningNumbers: MutableSet<Int> = mutableSetOf()

            numberRegex.findAll(input[0]).forEach {
                winningNumbers.add(it.value.toInt())
            }

            var matchingNumbers = -1

            numberRegex.findAll(input[1]).forEach {

                val number = it.value.toInt()

                if (number in winningNumbers) {
                    matchingNumbers++
                }
            }

            if (matchingNumbers >= 0) {
                score += (2.0).pow(matchingNumbers)
            }
        }

        return score
    }

    override fun solvePart2(): Any {

        parseInput()

        cards.forEach { (cardNumber, card) ->

            var matchingNumbers = 0

            card.cardNumbers.forEach {
                if (it in card.winningNumbers) {
                    matchingNumbers++
                }
            }

            if (matchingNumbers > 0) {
                for (i in cardNumber + 1..cardNumber + matchingNumbers) {
                    cards[i]?.let {
                        it.copies += card.copies
                    }
                }
            }
        }

        return cards.map { it.value.copies }.sum()
    }

    private fun parseInput() {

        val numberRegex = Regex("\\d+")

        IoUtils.forEachInputLines("day4") { line ->

            val cardNumber: Int = numberRegex.findAll(line.substring(0, line.indexOf(':'))).first().value.toInt()
            val input = line.substring(line.indexOf(':') + 1).split('|')

            val winningNumbers: MutableSet<Int> = mutableSetOf()

            numberRegex.findAll(input[0]).forEach {
                winningNumbers.add(it.value.toInt())
            }

            val cardNumbers = numberRegex.findAll(input[1]).map {
                it.value.toInt()
            }

            cards[cardNumber] = Card(1, winningNumbers, cardNumbers)
        }
    }

}

data class Card(var copies: Int, val winningNumbers: Set<Int>, val cardNumbers: Sequence<Int>)

fun main() {
    Day4().solve()
}