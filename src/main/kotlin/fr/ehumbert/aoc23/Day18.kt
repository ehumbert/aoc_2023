package fr.ehumbert.aoc23

import fr.ehumbert.aoc23.utils.Coordinates
import fr.ehumbert.aoc23.utils.Direction
import fr.ehumbert.aoc23.utils.IoUtils
import fr.ehumbert.aoc23.utils.getNextInDirection
import fr.ehumbert.aoc23.utils.manhattanDistance
import fr.ehumbert.aoc23.utils.picksTheorem

class Day18 : AbstractSolver() {

    override fun solvePart1(): Any {

        var pos = Coordinates(0, 0)

        val edges = mutableSetOf(pos)

        IoUtils.forEachInputLines("day18") { line ->

            val (direction, steps, _) = line.split(' ')

            pos = pos.getNextInDirection(Direction.fromLetter(direction), steps.toInt())

            if (pos !in edges) {
                edges.add(pos)
            }
        }

        var trench = 0L
        val coordinates = edges.toList()

        for (i in coordinates.indices) {

            val a = coordinates[i]
            val b = coordinates[(i + 1) % coordinates.size]

            trench += a.manhattanDistance(b)
        }

        return picksTheorem(edges.toList()) + trench
    }

    @OptIn(ExperimentalStdlibApi::class)
    override fun solvePart2(): Any {

        var pos = Coordinates(0, 0)

        val edges = mutableSetOf(pos)

        IoUtils.forEachInputLines("day18", false) { line ->

            val hex = Regex("\\(#(\\w+)\\)").find(line)!!
            val steps = hex.groupValues[1].substring(0..4).hexToInt()
            val direction = when(hex.groupValues[1][5]) {
                '0' -> Direction.RIGHT
                '1' -> Direction.DOWN
                '2' -> Direction.LEFT
                else -> Direction.UP
            }

            pos = pos.getNextInDirection(direction, steps)

            if (pos !in edges) {
                edges.add(pos)
            }
        }

        var trench = 0L
        val coordinates = edges.toList()

        for (i in coordinates.indices) {

            val a = coordinates[i]
            val b = coordinates[(i + 1) % coordinates.size]

            trench += a.manhattanDistance(b)
        }

        return picksTheorem(edges.toList()) + trench
    }

}

fun main() {
    Day18().solve()
}