package fr.ehumbert.aoc23

import fr.ehumbert.aoc23.utils.IoUtils

class Day7 : AbstractSolver() {

    override fun solvePart1(): Any {

        val hands: MutableList<Pair<String, Int>> = mutableListOf()

        IoUtils.forEachInputLines("day7") { line ->
            val (hand, bid) = line.split(' ')
            hands.add(Pair(hand, bid.toInt()))
        }

        hands.sortWith(
            compareBy<Pair<String, Int>> { getHandScore(it.first) }.then { a, b ->

                a.first.zip(b.first).forEach { (cardA, cardB) ->
                    val cardOrder = getCardOrderPart1(cardB) - getCardOrderPart1(cardA)
                    if (cardOrder != 0) {
                        return@then cardOrder
                    }
                }

                0
            }
        )

        var winnings = 0

        hands.forEachIndexed { i, hand ->
            winnings += (i + 1) * hand.second
        }

        return winnings
    }

    override fun solvePart2(): Any {

        val hands: MutableList<Pair<String, Int>> = mutableListOf()

        IoUtils.forEachInputLines("day7", false) { line ->
            val (hand, bid) = line.split(' ')
            hands.add(Pair(hand, bid.toInt()))
        }

        hands.sortWith(
            compareBy<Pair<String, Int>> {

                var hand = it.first

                if (hand.contains('J')) {
                    hand = findBestHandWithJoker(hand)
                }

                getHandScore(hand)
            }.then { a, b ->

                a.first.zip(b.first).forEach { (cardA, cardB) ->
                    val cardOrder = getCardOrderPart2(cardB) - getCardOrderPart2(cardA)
                    if (cardOrder != 0) {
                        return@then cardOrder
                    }
                }

                0
            }
        )

        var winnings = 0

        hands.forEachIndexed { i, hand ->
            winnings += (i + 1) * hand.second
        }

        return winnings
    }

    private fun getCardOrderPart1(card: Char): Int = when (card) {
        'A' -> 1
        'K' -> 2
        'Q' -> 3
        'J' -> 4
        'T' -> 5
        else -> 6 + (9 - card.digitToInt())
    }

    private fun getCardOrderPart2(card: Char): Int = when (card) {
        'A' -> 1
        'K' -> 2
        'Q' -> 3
        'J' -> 13
        'T' -> 4
        else -> 5 + (9 - card.digitToInt())
    }

    private fun getHandScore(hand: String): Int {

        val cardCount: MutableMap<Char, Int> = mutableMapOf()

        hand.forEach {
            cardCount[it] = cardCount.getOrDefault(it, 0) + 1
        }

        return when (cardCount.size) {
            5 -> 1 // High card
            4 -> 2 // One pair

            3 -> { // Two pair or Three of a kind
                if (cardCount.values.count { it == 2 } == 2) {
                    3 // Two pair
                } else {
                    4 // Three of a kind
                }
            }

            2 -> { // Four of a kind or Full house
                if (cardCount.values.any { it == 4 }) {
                    6 // Four of a kind
                } else {
                    5 // Full house
                }
            }

            else -> 7 // Five of a kind
        }
    }

    private fun findBestHandWithJoker(hand: String): String {

        val cardCount: MutableMap<Char, Int> = mutableMapOf()

        hand.forEach {
            cardCount[it] = cardCount.getOrDefault(it, 0) + 1
        }

        var mostFrequentCard: Pair<Char, Int>? = null
        var secondMostFrequentCard: Pair<Char, Int>? = null

        cardCount.forEach { (card, count) ->

            if (mostFrequentCard == null) {

                mostFrequentCard = Pair(card, count)

            } else if (mostFrequentCard!!.second < count) {

                secondMostFrequentCard = mostFrequentCard
                mostFrequentCard = Pair(card, count)

            } else if (secondMostFrequentCard == null) {

                secondMostFrequentCard = Pair(card, count)

            } else if (secondMostFrequentCard!!.second < count) {

                secondMostFrequentCard = Pair(card, count)
            }

        }

        var replacingCard = 'A'

        if (mostFrequentCard!!.first == 'J') {

            // If it's 5 J, we leave 'A' as the strongest card
            if (mostFrequentCard!!.second != 5) {
                replacingCard = secondMostFrequentCard!!.first
            }

        } else {

            replacingCard = mostFrequentCard!!.first
        }

        return hand.replace('J', replacingCard)
    }

}

fun main() {
    Day7().solve()
}