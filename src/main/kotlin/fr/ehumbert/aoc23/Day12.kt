package fr.ehumbert.aoc23

import fr.ehumbert.aoc23.utils.IoUtils
import fr.ehumbert.aoc23.utils.RegexUtils
import fr.ehumbert.aoc23.utils.combinations

class Day12 : AbstractSolver() {

    private val cache: MutableMap<Pair<String, List<Int>>, Long> = mutableMapOf()

    // Brute force over combinations
    override fun solvePart1(): Any {

        var result = 0

        IoUtils.forEachInputLines("day12") { line ->

            val (springs, info) = line.split(' ')
            val springsInfo = RegexUtils.digitRegex.findAll(info).map { it.value.toInt() }.toList()

            val possibleSlots = mutableListOf<Int>()
            springs.forEachIndexed { i, c ->
                if (c == '?') {
                    possibleSlots.add(i)
                }
            }

            val brokenSprings = springsInfo.sum() - springs.count { it == '#' }

            val combinations = possibleSlots.combinations(brokenSprings)

            combinations.forEach { permutation ->

                var arrangement = springs

                arrangement = arrangement.mapIndexed { index, c ->
                    if (c == '?') {
                        if (index in permutation) {
                            '#'
                        } else {
                            '.'
                        }
                    } else {
                        c
                    }
                }.joinToString("")


                if (arrangement.split('.').map { it.length }.filter { it != 0 }.toList() == springsInfo) {
                    result += 1
                }
            }
        }

        return result
    }

    // Recursion + cache
    override fun solvePart2(): Any {

        var result = 0L

        IoUtils.forEachInputLines("day12") { line ->

            val (springsToTransform, groups) = line.split(' ')
            val springGroupsToTransform = RegexUtils.digitRegex.findAll(groups).map { it.value.toInt() }

            var springs = springsToTransform
            val springGroups = mutableListOf<Int>()

            springGroups.addAll(springGroupsToTransform)

            for (i in 0..3) {
                springs += "?$springsToTransform"
                springGroups.addAll(springGroupsToTransform)
            }

            result += getCachedArrangements(springs, springGroups)
        }

        return result
    }

    private fun getCachedArrangements(springs: String, springGroups: List<Int>): Long {
        return cache.getOrPut(Pair(springs, springGroups)) { getPossibleArrangements(springs, springGroups) }
    }

    private fun getPossibleArrangements(springs: String, springGroups: List<Int>): Long {

        // Done
        if (springGroups.isEmpty()) {
            if (!springs.contains('#')) {
                return 1
            }
            return 0
        }

        val groupLength = springGroups.first()

        // Can't hold enough #
        if (springs.length < groupLength) {
            return 0
        }

        val spring = springs.first()

        // Skip .
        fun handleWorkingSpring() = getCachedArrangements(springs.substring(1), springGroups)

        fun handleBrokenSpring(): Long {

            val group = springs.substring(0..<groupLength).replace('?', '#')

            if (group != "#".repeat(groupLength)) {
                return 0
            }

            // Done
            if (springs.length == groupLength) {
                if (springGroups.size == 1) {
                    return 1
                }
                return 0
            }

            // Check if there's a separator at the end of the block
            if (springs[groupLength] in "?.") {

                val nextSprings = springs.substring(groupLength + 1)
                val nextGroups = springGroups.slice(1..<springGroups.size)

                return getCachedArrangements(nextSprings, nextGroups)
            }

            return 0
        }

        return when (spring) {
            '#' -> handleBrokenSpring()
            '.' -> handleWorkingSpring()
            else -> handleWorkingSpring() + handleBrokenSpring()
        }
    }

}

fun main() {
    Day12().solve()
}