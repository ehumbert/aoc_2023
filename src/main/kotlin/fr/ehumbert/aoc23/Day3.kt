package fr.ehumbert.aoc23

import fr.ehumbert.aoc23.utils.Coordinates
import fr.ehumbert.aoc23.utils.IoUtils

class Day3 : AbstractSolver() {

    private val schematic: MutableMap<Coordinates, Char> = mutableMapOf()

    private val neighbors: List<Coordinates> = listOf(
        Coordinates(0, 1),
        Coordinates(0, -1),
        Coordinates(1, 0),
        Coordinates(-1, 0),
        Coordinates(1, 1),
        Coordinates(-1, -1),
        Coordinates(-1, 1),
        Coordinates(1, -1)
    )

    override fun solvePart1(): Any {

        parseInput()

        val partNumbers: MutableList<Int> = mutableListOf()

        val numberBuffer: MutableList<Char> = mutableListOf()
        var numberHasSymbolNeighbor = false

        schematic.forEach { (coordinates, c) ->

            if (c.isDigit()) {

                numberBuffer.add(c)
                numberHasSymbolNeighbor = numberHasSymbolNeighbor || hasSymbolNeighbor(coordinates)

                return@forEach
            }

            if (numberBuffer.isNotEmpty()) {

                if (numberHasSymbolNeighbor) {
                    partNumbers.add(numberBuffer.joinToString("").toInt())
                }

                numberBuffer.clear()
                numberHasSymbolNeighbor = false
            }
        }

        return partNumbers.sum()
    }

    override fun solvePart2(): Any {

        var gearRatios = 0

        val numberBuffer: MutableList<NumberBuffer> = mutableListOf()

        val numberPositions: MutableList<NumberPart> = mutableListOf()
        val gearPositions: MutableList<Coordinates> = mutableListOf()

        schematic.forEach { (coordinates, c) ->

            if (c.isDigit()) {
                numberBuffer.add(NumberBuffer(c, coordinates))
                return@forEach
            }

            if (c == '*') {
                gearPositions.add(coordinates)
            }

            if (numberBuffer.isNotEmpty()) {

                var number = ""
                val xRange: MutableList<Long> = mutableListOf()
                val yRange: MutableList<Long> = mutableListOf()

                numberBuffer.forEach {
                    number += it.c
                    xRange.add(it.coordinates.first)
                    yRange.add(it.coordinates.second)
                }

                numberPositions.add(NumberPart(number.toInt(), xRange, yRange))

                numberBuffer.clear()
            }
        }

        gearPositions.forEach { gearCoordinates ->

            val adjacentNumbers: MutableSet<Int> = mutableSetOf()

            neighbors.forEach neighbors@{ move ->

                val neighborCoordinates = Coordinates(gearCoordinates.first + move.first, gearCoordinates.second + move.second)

                numberPositions.forEach { numberPart ->

                    if (neighborCoordinates.first in numberPart.xRange && neighborCoordinates.second in numberPart.yRange) {
                        adjacentNumbers.add(numberPart.number)
                    }

                    if (adjacentNumbers.size > 2) {
                        return@neighbors
                    }
                }
            }

            if (adjacentNumbers.size == 2) {
                gearRatios += adjacentNumbers.reduce { acc, number -> acc * number }
            }
        }

        return gearRatios
    }

    private fun parseInput() {

        var coordinates: Coordinates = Coordinates(0, 0)

        IoUtils.forEachInputLines("day3") { line ->

            coordinates = Coordinates(0, coordinates.second)

            line.forEach { c ->

                schematic[coordinates] = c

                coordinates = Coordinates(coordinates.first + 1, coordinates.second)
            }

            coordinates = Coordinates(coordinates.first, coordinates.second + 1)
        }
    }

    private fun hasSymbolNeighbor(coordinates: Coordinates): Boolean {

        neighbors.forEach { move ->

            val neighborCoordinates = Coordinates(coordinates.first + move.first, coordinates.second + move.second)
            val neighbor = schematic[neighborCoordinates]

            neighbor?.let {
                if (!it.isDigit() && it != '.') {
                    return true
                }
            }
        }

        return false
    }

}

data class NumberBuffer(val c: Char, val coordinates: Coordinates)
data class NumberPart(val number: Int, val xRange: List<Long>, val yRange: List<Long>)

fun main() {
    Day3().solve()
}