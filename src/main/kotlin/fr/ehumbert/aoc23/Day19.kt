package fr.ehumbert.aoc23

import fr.ehumbert.aoc23.utils.IoUtils
import kotlin.math.max
import kotlin.math.min

class Day19 : AbstractSolver() {

    // label : [instructions]
    private val instructions: MutableMap<String, List<String>> = mutableMapOf()

    private val paths: MutableList<List<ParsedInstruction>> = mutableListOf()

    override fun solvePart1(): Any {

        var rating = 0

        var isInstruction = true

        IoUtils.forEachInputLines("day19") { line ->

            if (line.isBlank()) {
                isInstruction = false
                return@forEachInputLines
            }

            if (isInstruction) {
                val match = Regex("(\\w+)\\{(.+)}").find(line)!!.groupValues
                instructions[match[1]] = match[2].split(',')
                return@forEachInputLines
            }

            val states: MutableMap<String, Int> = mutableMapOf()

            Regex("(\\w)=(\\d+)").findAll(line).forEach { match ->
                states[match.groupValues[1]] = match.groupValues[2].toInt()
            }

            var next = "in"

            while (next !in listOf("A", "R")) {
                for (instruction in instructions[next]!!) {
                    val redirect = parseInstruction(instruction, states)

                    if (redirect.first) {
                        next = redirect.second
                        break
                    }
                }
            }

            if (next == "A") {
                rating += states.values.sum()
            }
        }

        return rating
    }

    override fun solvePart2(): Any {

        exploreInstructions("in")

        var possibilities = 0L

        paths.forEach { path ->
            possibilities += scorePath(path)
        }

        return possibilities
    }

    // (shouldRedirect, redirect)
    private fun parseInstruction(instruction: String, states: Map<String, Int>): Pair<Boolean, String> {

        Regex("(\\w)([<>])(\\d+):(\\w+)").find(instruction)?.let {

            val (state, operator, number, redirect) = it.groupValues.slice(1..4)

            return when (operator) {
                "<" -> Pair(states[state]!! < number.toInt(), redirect)
                ">" -> Pair(states[state]!! > number.toInt(), redirect)
                else -> throw RuntimeException("Unknown operator $operator")
            }
        }

        return Pair(true, instruction)
    }

    private fun exploreInstructions(label: String, path: List<ParsedInstruction> = listOf()) {

        if (label == "A") {
            paths.add(path)
            return
        }

        if (label == "R") {
            return
        }

        val currentPath = path.toMutableList()

        instructions[label]!!.forEach { instruction ->

            val match = Regex("(\\w)([<>])(\\d+):(\\w+)").find(instruction)

            if (match != null) {

                val (state, operator, number, redirect) = match.groupValues.slice(1..4)
                val parsed = ParsedInstruction(state, operator, number.toInt())

                exploreInstructions(redirect, currentPath + parsed)

                currentPath.add(parsed.reverse())
            } else {
                exploreInstructions(instruction, currentPath.toList())
            }
        }
    }

    private fun scorePath(path: List<ParsedInstruction>): Long {

        val boundaries: MutableMap<String, Pair<Int, Int>> = mutableMapOf(
            "x" to Pair(1, 4000),
            "m" to Pair(1, 4000),
            "a" to Pair(1, 4000),
            "s" to Pair(1, 4000)
        )

        path.forEach { instruction ->

            val range = boundaries[instruction.state]!!

            var rangeStart = range.first
            var rangeEnd = range.second

            when (instruction.operator) {
                ">" -> rangeStart = max(rangeStart, instruction.number + 1)
                "<" -> rangeEnd = min(rangeEnd, instruction.number - 1)
            }

            boundaries[instruction.state] = Pair(rangeStart, rangeEnd)
        }

        var combinations = 1L
        boundaries.values.forEach { range ->
            combinations *= range.second - range.first + 1
        }

        return combinations
    }

}

data class ParsedInstruction(val state: String, val operator: String, val number: Int)

fun ParsedInstruction.reverse() = ParsedInstruction(
    this.state,
    if (this.operator == "<") ">" else "<",
    if (this.operator == "<") this.number - 1 else this.number + 1
)

fun main() {
    Day19().solve()
}
