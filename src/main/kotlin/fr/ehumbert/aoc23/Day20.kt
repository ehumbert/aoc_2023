package fr.ehumbert.aoc23

import fr.ehumbert.aoc23.utils.IoUtils
import fr.ehumbert.aoc23.utils.MathsUtils

class Day20 : AbstractSolver() {

    private val modules: MutableMap<String, Module> = mutableMapOf()

    override fun solvePart1(): Any {

        var lowPulses = 0L
        var highPulses = 0L

        val pulses = mutableListOf<Pulse>()

        val conjunctionModules: MutableList<ConjunctionModule> = mutableListOf()

        IoUtils.forEachInputLines("day20") { line ->

            val match = Regex("([%&]?\\w+) -> (.+)").find(line)!!.groupValues
            val (module, output) = match.slice(1..2)

            val outputs = output.split(',').map { it.trim() }
            val moduleType = ModuleType.getTypeFromString(module)
            val moduleName = when (moduleType) {
                ModuleType.BROADCAST -> module
                else -> module.substring(1)
            }

            modules[moduleName] = when (moduleType) {

                ModuleType.BROADCAST -> BroadCastModule(moduleName, outputs)

                ModuleType.CONJUNCTION -> {
                    val cm = ConjunctionModule(moduleName, outputs)
                    conjunctionModules.add(cm)
                    cm
                }

                ModuleType.FLIP_FLOP -> FlipFlopModule(moduleName, outputs)
            }
        }

        // parse inputs for conjunction modules
        conjunctionModules.forEach { cm ->
            modules.values.filter { it.outputs.contains(cm.name) }.forEach {
                cm.inputs[it.name] = false
            }
        }

        repeat(1000) {

            pulses.add(Triple(false, "button", "broadcaster"))

            while (pulses.isNotEmpty()) {

                val pulse = pulses.removeFirst()

                if (pulse.first) {
                    highPulses++
                } else {
                    lowPulses++
                }

                modules[pulse.third]?.receivePulse(pulse)?.let {
                    pulses.addAll(it)
                }
            }
        }

        return lowPulses * highPulses
    }

    override fun solvePart2(): Any {

        val pulses = mutableListOf<Pulse>()

        val conjunctionModules: MutableList<ConjunctionModule> = mutableListOf()
        val lines = mutableListOf("flowchart TD")

        IoUtils.forEachInputLines("day20") { line ->

            val match = Regex("([%&]?\\w+) -> (.+)").find(line)!!.groupValues
            val (module, output) = match.slice(1..2)

            val outputs = output.split(',').map { it.trim() }
            val moduleType = ModuleType.getTypeFromString(module)
            val moduleName = when (moduleType) {
                ModuleType.BROADCAST -> module
                else -> module.substring(1)
            }

            outputs.forEach {
                lines.add("    $moduleName --> $it")
            }

            modules[moduleName] = when (moduleType) {

                ModuleType.BROADCAST -> BroadCastModule(moduleName, outputs)

                ModuleType.CONJUNCTION -> {
                    val cm = ConjunctionModule(moduleName, outputs)
                    conjunctionModules.add(cm)
                    cm
                }

                ModuleType.FLIP_FLOP -> FlipFlopModule(moduleName, outputs)
            }
        }

        IoUtils.writeLines("/tmp/mermaid.txt", lines)

        // parse inputs for conjunction modules
        conjunctionModules.forEach { cm ->
            modules.values.filter { it.outputs.contains(cm.name) }.forEach {
                cm.inputs[it.name] = false
            }
        }

        var i = 1L
        var done = false

        val conjunctionsCycle = mutableListOf<Long>()

        while (!done) {

            pulses.add(Triple(false, "button", "broadcaster"))

            while (pulses.isNotEmpty()) {

                val pulse = pulses.removeFirst()

                if (pulse.first && pulse.second in listOf("xf", "cm", "sz", "gc")) {

                    conjunctionsCycle.add(i)

                    if (conjunctionsCycle.size == 4) {
                        done = true
                        break
                    }
                }

                modules[pulse.third]?.receivePulse(pulse)?.let {
                    pulses.addAll(it)
                }
            }

            i++
        }

        return MathsUtils.findLcm(conjunctionsCycle)
    }

}

abstract class Module(val name: String, val outputs: List<String>) {

    abstract val moduleType: ModuleType

    abstract fun receivePulse(pulse: Pulse): List<Pulse>

}

class FlipFlopModule(name: String, outputs: List<String>) : Module(name, outputs) {

    private var isOn = false

    override val moduleType = ModuleType.FLIP_FLOP

    override fun receivePulse(pulse: Pulse): List<Pulse> {

        if (!pulse.first) {
            this.isOn = !this.isOn
            return outputs.map { Pulse(this.isOn, name, it) }
        }

        return listOf()
    }

}

class ConjunctionModule(name: String, outputs: List<String>) : Module(name, outputs) {

    val inputs: MutableMap<String, Boolean> = mutableMapOf()

    override val moduleType = ModuleType.CONJUNCTION

    override fun receivePulse(pulse: Pulse): List<Pulse> {

        inputs[pulse.second] = pulse.first
        val nextPulse = inputs.any { !it.value }

        return outputs.map { Pulse(nextPulse, name, it) }
    }

}

class BroadCastModule(name: String, outputs: List<String>) : Module(name, outputs) {

    override val moduleType = ModuleType.BROADCAST

    override fun receivePulse(pulse: Pulse): List<Pulse> {
        return outputs.map { Pulse(pulse.first, name, it) }
    }

}

enum class ModuleType(val start: String) {

    BROADCAST("broadcast"),
    CONJUNCTION("&"),
    FLIP_FLOP("%");

    companion object {
        fun getTypeFromString(name: String) = ModuleType.entries.first { name.startsWith(it.start) }
    }

}

// pulse (false == low pulse), from, to
typealias Pulse = Triple<Boolean, String, String>

fun main() {
    Day20().solve()
}
