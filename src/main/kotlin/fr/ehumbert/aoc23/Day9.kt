package fr.ehumbert.aoc23

import fr.ehumbert.aoc23.utils.IoUtils
import fr.ehumbert.aoc23.utils.RegexUtils

class Day9 : AbstractSolver() {

    override fun solvePart1(): Any {

        var sum: Long = 0

        IoUtils.forEachInputLines("day9") { line ->

            val allNumbers: MutableList<MutableList<Long>> = mutableListOf()

            val numbers = RegexUtils.digitRegex.findAll(line).map { it.value.toLong() }.toMutableList()
            val newNumbers: MutableList<Long> = mutableListOf()

            allNumbers.add(numbers.toMutableList())

            var isFinished = false

            while (!isFinished) {

                for (i in numbers.indices) {
                    if (i + 1 < numbers.size) {
                        newNumbers.add(numbers[i + 1] - numbers[i])
                    }
                }

                allNumbers.add(newNumbers.toMutableList())

                if (newNumbers.all { it == 0L }) {
                    isFinished = true
                }

                numbers.clear()
                numbers.addAll(newNumbers)
                newNumbers.clear()
            }

            for (i in allNumbers.size - 1 downTo 1) {

                val currentNumbers = allNumbers[i]
                val nextNumbers = allNumbers[i - 1]

                // Skipping the 0 addition as I don't see the need for that
                nextNumbers.add(currentNumbers.last() + nextNumbers.last())
            }

            sum += allNumbers[0].last()
        }

        return sum
    }

    override fun solvePart2(): Any {

        var sum: Long = 0

        IoUtils.forEachInputLines("day9") { line ->

            val allNumbers: MutableList<MutableList<Long>> = mutableListOf()

            val numbers = RegexUtils.digitRegex.findAll(line).map { it.value.toLong() }.toMutableList()
            val newNumbers: MutableList<Long> = mutableListOf()

            allNumbers.add(numbers.toMutableList())

            var isFinished = false

            while (!isFinished) {

                for (i in numbers.indices) {
                    if (i + 1 < numbers.size) {
                        newNumbers.add(numbers[i + 1] - numbers[i])
                    }
                }

                allNumbers.add(newNumbers.toMutableList())

                if (newNumbers.all { it == 0L }) {
                    isFinished = true
                }

                numbers.clear()
                numbers.addAll(newNumbers)
                newNumbers.clear()
            }

            for (i in allNumbers.size - 1 downTo 1) {

                val currentNumbers = allNumbers[i]
                val nextNumbers = allNumbers[i - 1]

                // Skipping the 0 addition as I don't see the need for that
                nextNumbers.addFirst(nextNumbers.first() - currentNumbers.first())
            }

            sum += allNumbers[0].first()
        }

        return sum
    }

}

fun main() {
    Day9().solve()
}