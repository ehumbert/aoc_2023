package fr.ehumbert.aoc23

import fr.ehumbert.aoc23.utils.IoUtils
import kotlin.math.min


class Day5 : AbstractSolver() {

    private val seedNumbers: MutableList<Long> = mutableListOf()

    private val seedToSoilMap: MutableList<Transformation> = mutableListOf()
    private val soilToFertilizerMap: MutableList<Transformation> = mutableListOf()
    private val fertilizerToWaterMap: MutableList<Transformation> = mutableListOf()
    private val waterToLightMap: MutableList<Transformation> = mutableListOf()
    private val lightToTemperatureMap: MutableList<Transformation> = mutableListOf()
    private val temperatureToHumidityMap: MutableList<Transformation> = mutableListOf()
    private val humidityToLocationMap: MutableList<Transformation> = mutableListOf()

    private val almanac = arrayOf(
        seedToSoilMap,
        soilToFertilizerMap,
        fertilizerToWaterMap,
        waterToLightMap,
        lightToTemperatureMap,
        temperatureToHumidityMap,
        humidityToLocationMap
    )

    override fun solvePart1(): Any {

        parseInput()

        val closest = seedNumbers.minOf { seed ->

            var number = seed

            almanac.forEach transformer@{ currentMap ->
                // If there's no mapping between source and destination -> the number stays the same
                currentMap.forEach { transformation ->
                    if (number in transformation.sourceRange) {
                        val offset = number - transformation.sourceRange.first
                        number = transformation.destinationRange.first + offset
                        return@transformer
                    }
                }
            }

            number
        }

        return closest
    }

    override fun solvePart2(): Any {

        val seedRanges: MutableSet<LongRange> = mutableSetOf()
        val nextSeedRanges: MutableSet<LongRange> = mutableSetOf()

        for (i in 0..<seedNumbers.size step 2) {
            seedRanges.add(seedNumbers[i]..<seedNumbers[i] + seedNumbers[i + 1])
        }

        almanac.forEach transformer@{ currentMap ->

            seedRanges.forEach {

                val toTransform: MutableSet<LongRange> = mutableSetOf(it)
                val transformed: MutableSet<LongRange> = mutableSetOf()

                currentMap.forEach mapEntry@{ transformation ->

                    val toTransformBuffer: MutableSet<LongRange> = mutableSetOf()

                    toTransform.forEach { seedRange ->

                        val offset = transformation.destinationRange.first - transformation.sourceRange.first

                        if (seedRange.first in transformation.sourceRange && seedRange.last in transformation.sourceRange) {

                            // Fully included
                            transformed.add(seedRange.first + offset..seedRange.last + offset)

                        } else if (transformation.sourceRange.first in seedRange && transformation.sourceRange.last in seedRange) {

                            // Fully intersect
                            toTransformBuffer.add(seedRange.first..<transformation.sourceRange.first)
                            transformed.add(transformation.destinationRange.first..transformation.destinationRange.last)
                            toTransformBuffer.add(transformation.sourceRange.last + 1..seedRange.last)

                        } else if (transformation.sourceRange.first in seedRange) {

                            // Right intersect
                            toTransformBuffer.add(seedRange.first..<transformation.sourceRange.first)
                            transformed.add(transformation.destinationRange.first..seedRange.last + offset)

                        } else if (transformation.sourceRange.last in seedRange) {

                            // Left intersect
                            transformed.add(seedRange.first + offset..transformation.destinationRange.last)
                            toTransformBuffer.add(transformation.sourceRange.last + 1..seedRange.last)

                        } else {

                            // No intersect
                            toTransformBuffer.add(seedRange.first..seedRange.last)
                        }
                    }

                    toTransform.clear()
                    toTransform.addAll(toTransformBuffer)
                }

                nextSeedRanges.addAll(toTransform + transformed)
            }

            seedRanges.clear()
            seedRanges.addAll(nextSeedRanges)
            nextSeedRanges.clear()
        }

        val closest = seedRanges.minBy { it.first }.first

        return closest
    }

    /*
     * First implementation of part 2 with a "skip" mechanic (still bruteforce).
     * Keeping it as a memento
     */
    fun solvePart2SemiBruteForce(): String {

        var closest = Long.MAX_VALUE

        for (i in 0..<seedNumbers.size step 2) {

            val seedRange = seedNumbers[i]..<seedNumbers[i] + seedNumbers[i + 1]
            var canSkipRange: Long? = null

            for (seed in seedRange) {

                var number = seed

                // Skip useless seeds
                if (canSkipRange != null) {
                    if (seed < seedRange.first + canSkipRange) {
                        continue
                    } else {
                        canSkipRange = null
                    }
                }

                almanac.forEach transformer@{ currentMap ->
                    // If there's no mapping between source and destination -> the number stays the same
                    currentMap.forEach { transformation ->
                        if (number in transformation.sourceRange) {

                            val maximumSkip = transformation.sourceRange.last - number

                            canSkipRange = if (canSkipRange == null) maximumSkip else min(canSkipRange!!, maximumSkip)

                            val offset = number - transformation.sourceRange.first
                            number = transformation.destinationRange.first + offset

                            return@transformer
                        }
                    }
                }

                closest = min(closest, number)
            }
        }

        return closest.toString()
    }

    private fun parseInput() {

        val numberRegex = Regex("\\d+")
        var currentMap = seedToSoilMap

        IoUtils.forEachInputLines("day5") parser@{ line ->

            if (line.isBlank()) {
                return@parser
            }

            if (line.startsWith("seeds:")) {
                numberRegex.findAll(line).forEach {
                    seedNumbers.add(it.value.toLong())
                }
                return@parser
            }

            if (line.startsWith("seed-to-soil map:")) {
                currentMap = seedToSoilMap
                return@parser
            }

            if (line.startsWith("soil-to-fertilizer map:")) {
                currentMap = soilToFertilizerMap
                return@parser
            }

            if (line.startsWith("fertilizer-to-water map:")) {
                currentMap = fertilizerToWaterMap
                return@parser
            }

            if (line.startsWith("water-to-light map:")) {
                currentMap = waterToLightMap
                return@parser
            }

            if (line.startsWith("light-to-temperature map:")) {
                currentMap = lightToTemperatureMap
                return@parser
            }

            if (line.startsWith("temperature-to-humidity map:")) {
                currentMap = temperatureToHumidityMap
                return@parser
            }

            if (line.startsWith("humidity-to-location map:")) {
                currentMap = humidityToLocationMap
                return@parser
            }

            val (destinationRangeStart, sourceRangeStart, rangeLength) = numberRegex.findAll(line).map {
                it.value.toLong()
            }.toList()

            currentMap.add(
                Transformation(
                    sourceRange = sourceRangeStart..<sourceRangeStart + rangeLength,
                    destinationRange = destinationRangeStart..<destinationRangeStart + rangeLength
                )
            )
        }
    }

}

data class Transformation(val sourceRange: LongRange, val destinationRange: LongRange)

fun main() {
    Day5().solve()
}