package fr.ehumbert.aoc23

import fr.ehumbert.aoc23.utils.Coordinates
import fr.ehumbert.aoc23.utils.Direction
import fr.ehumbert.aoc23.utils.Grid
import fr.ehumbert.aoc23.utils.IoUtils
import fr.ehumbert.aoc23.utils.getNextInDirection
import kotlin.math.max

class Day21 : AbstractSolver() {

    private val grid = Grid(IoUtils.readAllInputLines("day21"))

    private val visited: MutableMap<Coordinates, Int> = mutableMapOf()
    private val possibleCoordinates: MutableSet<Coordinates> = mutableSetOf()

    override fun solvePart1(): Any {

        val start = grid.findFirstInGrid('S')!!

        explore(start, 64)

        return possibleCoordinates.size
    }

    /*
     * I failed solving part2 on my own and went for a hint thread on reddit where 2 solutions were promoted
     * 1) https://github.com/villuna/aoc23/wiki/A-Geometric-solution-to-advent-of-code-2023,-day-21
     * 2) Quadratic polynomial ax² + bx + c
     *
     * Chose the 2) so that I can reuse the part 1 code
     * to find the coefficients we need 3 points
     * 26501365 = 65 + 202300 * 131
     * n1 = 65 (length from mid to edge of the square)
     * n2 = 65 + 131 = 196 (n1 + width of grid)
     * n3 = 65 + (131 * 2) = 327
     */
    override fun solvePart2(): Any {

        visited.clear()
        possibleCoordinates.clear()

        val start = grid.findFirstInGrid('S')!!

        explore(start, 65, true)
        val polynomial1 = possibleCoordinates.size.toLong()

        visited.clear()
        possibleCoordinates.clear()

        explore(start, 196, true)
        val polynomial2 = possibleCoordinates.size.toLong()

        visited.clear()
        possibleCoordinates.clear()

        explore(start, 327, true)
        val polynomial3 = possibleCoordinates.size.toLong()

        val n: Long = 26501365 / 131

        val a: Long = (polynomial3 + polynomial1 - 2 * polynomial2) / 2
        val b: Long = polynomial2 - polynomial1 - a
        val c: Long = polynomial1

        return a * n * n + b * n + c
    }

    private fun explore(coordinates: Coordinates, steps: Int, part2: Boolean = false) {

        val visit = visited.getOrDefault(coordinates, 0)

        // Already went here with fewer steps
        if (coordinates in visited && steps <= visit) {
            return
        }

        visited[coordinates] = max(visit, steps)

        if (steps % 2 == 0) {
            possibleCoordinates.add(coordinates)
        }

        if (steps == 0) {
            return
        }

        Direction.entries.forEach { direction ->

            val next = coordinates.getNextInDirection(direction)
            val nextTile = grid.getTile(if (part2) getInfiniteCoordinates(next) else next)

            if (nextTile != null && nextTile != '#') {
                explore(next, steps - 1, part2)
            }
        }
    }

    private fun getInfiniteCoordinates(coordinates: Coordinates): Coordinates {

        val modulo = grid.maxX + 1

        return Pair(((coordinates.first % modulo) + modulo) % modulo, ((coordinates.second % modulo) + modulo) % modulo)
    }

}

fun main() {
    Day21().solve()
}
