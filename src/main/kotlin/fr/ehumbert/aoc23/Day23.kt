package fr.ehumbert.aoc23

import fr.ehumbert.aoc23.utils.Coordinates
import fr.ehumbert.aoc23.utils.Direction
import fr.ehumbert.aoc23.utils.Grid
import fr.ehumbert.aoc23.utils.IoUtils
import fr.ehumbert.aoc23.utils.getNextInDirection
import kotlin.math.max

class Day23 : AbstractSolver() {

    private val grid = Grid(IoUtils.readAllInputLines("day23"))
    private var maxPath = 0

    override fun solvePart1(): Any {

        val start = Coordinates(1, 0)

        dfs(start)

        return maxPath
    }

    // bruteforce with debugger open to avoid waiting until the end of the execution
    override fun solvePart2(): Any {

        maxPath = 0

        val start = Coordinates(1, 0)

        dfs2(start)

        return maxPath
    }

    private fun dfs(coordinates: Coordinates, seen: Set<Coordinates> = setOf()) {

        if (coordinates == Coordinates(grid.maxX - 1, grid.maxY)) {
            maxPath = max(maxPath, seen.size)
        }

        val nextCoordinates: List<Coordinates> = when(grid.getTile(coordinates)) {
            '>' -> listOf(coordinates.getNextInDirection(Direction.RIGHT))
            '<' -> listOf(coordinates.getNextInDirection(Direction.LEFT))
            '^' -> listOf(coordinates.getNextInDirection(Direction.UP))
            'v' -> listOf(coordinates.getNextInDirection(Direction.DOWN))
            else -> Direction.entries.map { direction -> coordinates.getNextInDirection(direction) }
        }

        nextCoordinates.forEach { next ->

            val nextTile = grid.getTile(next)

            if (next !in seen && nextTile != null && nextTile != '#') {
                dfs(next, seen + setOf(coordinates))
            }
        }
    }

    private fun dfs2(coordinates: Coordinates, seen: Set<Coordinates> = setOf()) {

        if (coordinates == Coordinates(grid.maxX - 1, grid.maxY)) {
            maxPath = max(maxPath, seen.size)
        }

        // at last intersection before the end, only take the path to the end (obviously input dependant)
        val nextCoordinates: List<Coordinates> = if (coordinates == Coordinates(125, 129)) {
            listOf(Coordinates(125, 130))
        } else {
            Direction.entries.map { direction -> coordinates.getNextInDirection(direction) }
        }

        nextCoordinates.forEach { next ->

            val nextTile = grid.getTile(next)

            if (next !in seen && nextTile != null && nextTile != '#') {
                dfs2(next, seen + setOf(coordinates))
            }
        }
    }

}

fun main() {
    Day23().solve()
}