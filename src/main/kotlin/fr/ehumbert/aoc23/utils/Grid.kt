package fr.ehumbert.aoc23.utils

import kotlin.math.max

class Grid(lines: Iterable<String>) {

    private val grid: MutableMap<Coordinates, Char> = mutableMapOf()

    var maxX = 0L
    var maxY = 0L

    init {
        lines.forEachIndexed { y, line ->
            maxY = max(maxY, y.toLong())
            line.forEachIndexed { x, tile ->
                maxX = max(maxX, x.toLong())
                putTile(Coordinates(x.toLong(), y.toLong()), tile)
            }
        }
    }

    fun putTile(coordinates: Coordinates, tile: Char) {
        grid[coordinates] = tile
    }

    fun getTile(coordinates: Coordinates): Char? {
        return grid[coordinates]
    }

    fun getNextInDirection(coordinates: Coordinates, direction: Direction) = when (direction) {
        Direction.RIGHT -> Coordinates(coordinates.first + 1, coordinates.second)
        Direction.LEFT -> Coordinates(coordinates.first - 1, coordinates.second)
        Direction.UP -> Coordinates(coordinates.first, coordinates.second - 1)
        Direction.DOWN -> Coordinates(coordinates.first, coordinates.second + 1)
    }

    fun getRight(coordinates: Coordinates): Coordinates {
        return Coordinates(coordinates.first + 1, coordinates.second)
    }

    fun getLeft(coordinates: Coordinates): Coordinates {
        return Coordinates(coordinates.first - 1, coordinates.second)
    }

    fun getUp(coordinates: Coordinates): Coordinates {
        return Coordinates(coordinates.first, coordinates.second - 1)
    }

    fun getDown(coordinates: Coordinates): Coordinates {
        return Coordinates(coordinates.first, coordinates.second + 1)
    }

    fun findFirstInGrid(element: Char): Coordinates? {

        grid.forEach { (coordinates, tile) ->
            if (tile == element) {
                return coordinates
            }
        }

        return null
    }

    fun findAllInGrid(element: Char): List<Coordinates> {

        val finds = mutableListOf<Coordinates>()

        grid.forEach { (coordinates, tile) ->
            if (tile == element) {
                finds.add(coordinates)
            }
        }

        return finds
    }

    fun displayGrid(transformTile: (coordinates: Coordinates, tile: Char) -> Char = { _, c -> c }) {


        for (y in 0..maxY) {

            var line = ""

            for (x in 0..maxX) {

                val coordinates = Coordinates(x, y)
                val tile = getTile(coordinates)

                val toDisplay = transformTile(coordinates, tile!!)

                line += toDisplay
            }

            println(line)
        }
    }

    fun iterateGrid(func: (coordinates: Coordinates, tile: Char) -> Unit) {

        for (y in 0..maxY) {

            for (x in 0..maxX) {

                val coordinates = Coordinates(x, y)
                val tile = getTile(coordinates)

                func(coordinates, tile!!)
            }
        }
    }

    fun reverseIterateGrid(func: (coordinates: Coordinates, tile: Char) -> Unit) {

        for (y in maxY downTo 0) {

            for (x in maxX downTo 0) {

                val coordinates = Coordinates(x, y)
                val tile = getTile(coordinates)

                func(coordinates, tile!!)
            }
        }
    }

}

