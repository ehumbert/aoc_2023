package fr.ehumbert.aoc23.utils

import kotlin.math.abs

typealias Coordinates = Pair<Long, Long>

fun Coordinates.manhattanDistance(target: Coordinates) = abs(target.first - this.first) + abs(target.second - this.second)

fun Coordinates.getNextInDirection(direction: Direction, steps: Int = 1) = when (direction) {
    Direction.RIGHT -> Coordinates(this.first + 1 * steps, this.second)
    Direction.LEFT -> Coordinates(this.first - 1 * steps, this.second)
    Direction.UP -> Coordinates(this.first, this.second - 1 * steps)
    Direction.DOWN -> Coordinates(this.first, this.second + 1 * steps)
}

data class Coordinates3D(val x: Int, val y: Int, var z: Int)