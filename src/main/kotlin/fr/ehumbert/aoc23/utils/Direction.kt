package fr.ehumbert.aoc23.utils

enum class Direction(val dir: String) {
    RIGHT("R"),
    LEFT("L"),
    UP("U"),
    DOWN("D");

    companion object {

        fun fromLetter(direction: String): Direction {
            return Direction.entries.first { it.dir == direction }
        }

    }

}