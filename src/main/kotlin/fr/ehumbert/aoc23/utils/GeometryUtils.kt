package fr.ehumbert.aoc23.utils

import kotlin.math.abs

fun shoelaceArea(coordinates: List<Coordinates>): Long {

    var shoelace = 0L

    for (i in coordinates.indices) {

        val a = coordinates[i]
        val b = coordinates[(i + 1) % coordinates.size]

        shoelace += (a.first * b.second) - (a.second * b.first)
    }

    return abs(shoelace) / 2
}

fun picksTheorem(coordinates: List<Coordinates>): Long {

    var edgeNumbers = 0L

    for (i in coordinates.indices) {

        val a = coordinates[i]
        val b = coordinates[(i + 1) % coordinates.size]

        edgeNumbers += a.manhattanDistance(b)
    }

    return shoelaceArea(coordinates) - (edgeNumbers / 2) + 1
}