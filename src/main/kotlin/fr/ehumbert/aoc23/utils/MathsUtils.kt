package fr.ehumbert.aoc23.utils

class MathsUtils {

    companion object {

        fun findLcm(numbers: List<Long>): Long {

            var result = numbers.first()

            for (i in 1..<numbers.size) {
                result = lcm(result, numbers[i])
            }

            return result
        }

        fun findGcd(numbers: List<Long>): Long {

            var result = numbers.first()

            for (i in 1..<numbers.size) {
                result = gcd(result, numbers[i])
            }

            return result
        }

        private fun lcm(a: Long, b: Long) = a * (b / gcd(a, b))

        private fun gcd(a: Long, b: Long): Long = if (b == 0L) a else gcd(b, a % b)

    }

}