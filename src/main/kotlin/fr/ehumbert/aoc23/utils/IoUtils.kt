package fr.ehumbert.aoc23.utils

import java.io.BufferedReader
import java.io.File

class IoUtils {

    companion object {

        fun forEachInputLines(day: String, isTest: Boolean = false, func: (line: String) -> Unit) {

            getInputBufferedReader(day, isTest).forEachLine {
                func(it)
            }
        }

        fun readAllInputLines(day: String, isTest: Boolean = false): List<String> {

            return getInputBufferedReader(day, isTest).readLines()
        }

        fun writeLines(path: String, lines: List<String>) {

            File(path).printWriter().use {
                lines.forEach { line ->
                    it.println(line)
                    it.flush()
                }
            }
        }

        private fun getInputBufferedReader(day: String, isTest: Boolean): BufferedReader {

            val file = if (isTest) "$day/test.txt" else "$day/input.txt"

            val inputStream = this::class.java.classLoader.getResourceAsStream(file)
                ?: throw RuntimeException("$file not found")

            return inputStream.bufferedReader()
        }

    }

}
