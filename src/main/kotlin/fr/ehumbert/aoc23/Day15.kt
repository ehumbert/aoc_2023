package fr.ehumbert.aoc23

import fr.ehumbert.aoc23.utils.IoUtils

class Day15 : AbstractSolver() {

    private val regex = Regex("(\\w+)(=\\d+|-)")

    override fun solvePart1(): Any {

        var result = 0

        IoUtils.forEachInputLines("day15") { line ->

            line.split(',').forEach { s ->

                var hash = 0

                s.forEach { c ->
                    hash = ((hash + c.code) * 17) % 256
                }

                result += hash
            }
        }

        return result
    }

    override fun solvePart2(): Any {

        val boxes: Array<MutableMap<String, Int>> = Array(256) { mutableMapOf() }

        IoUtils.forEachInputLines("day15") { line ->

            line.split(',').forEach { s ->

                val matches = regex.find(s)!!.groupValues
                val label = matches[1]
                val action = matches[2]

                var hash = 0

                label.forEach { c ->
                    hash = ((hash + c.code) * 17) % 256
                }

                when (action.length) {
                    1 -> boxes[hash].remove(label)
                    else -> boxes[hash][label] = action.substring(1).toInt()
                }
            }
        }

        var result = 0

        boxes.forEachIndexed { i, box ->
            box.entries.forEachIndexed { j, (_, focalLength) ->
                result += (i + 1) * (j + 1) * focalLength
            }
        }

        return result
    }

}

fun main() {
    Day15().solve()
}