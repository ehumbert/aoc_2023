package fr.ehumbert.aoc23

import fr.ehumbert.aoc23.utils.Coordinates
import fr.ehumbert.aoc23.utils.Direction
import fr.ehumbert.aoc23.utils.Grid
import fr.ehumbert.aoc23.utils.IoUtils
import kotlin.math.abs

class Day10 : AbstractSolver() {

    private val grid: Grid = Grid(IoUtils.readAllInputLines("day10"))
    private val pipes: MutableList<Coordinates> = mutableListOf()
    private val visited: MutableSet<Coordinates> = mutableSetOf()

    private val validPipes = listOf('|', '-', 'L', 'J', '7', 'F', 'S')
    private val validPipeTransition = mapOf(
        Direction.RIGHT to listOf('-', 'J', '7'),
        Direction.LEFT to listOf('-', 'L', 'F'),
        Direction.UP to listOf('|', 'F', '7'),
        Direction.DOWN to listOf('|', 'J', 'L')
    )

    override fun solvePart1(): Any {

        val start = grid.findFirstInGrid('S')!!
        constructPipe(start)

        displayPipes()

        return pipes.size / 2
    }

    override fun solvePart2(): Any {

        var shoelace = 0L

        for (i in 0..<pipes.size) {

            val a = pipes[i]
            val b = pipes[(i + 1) % pipes.size]

            shoelace += ((a.first * b.second) - (a.second * b.first))
        }

        // Shoelace formula
        val area = abs(shoelace) / 2

        // Pick's theorem
        val includedPoints = area - (pipes.size / 2) + 1

        return includedPoints
    }

    private fun constructPipe(start: Coordinates) {

        val toVisit: MutableList<Coordinates> = mutableListOf()
        toVisit.add(start)

        while (toVisit.isNotEmpty()) {

            val coordinates = toVisit.removeLast()

            if (coordinates in visited) {
                continue
            }

            visited.add(coordinates)

            val tile = grid.getTile(coordinates)
            if (tile !in validPipes) {
                continue
            }

            pipes.add(coordinates)

            val right = grid.getRight(coordinates)
            val rightTile = grid.getTile(right)

            if (rightTile in getValidNextTile(tile!!, Direction.RIGHT)) {
                toVisit.add(right)
            }

            val left = grid.getLeft(coordinates)
            val leftTile = grid.getTile(left)

            if (leftTile in getValidNextTile(tile, Direction.LEFT)) {
                toVisit.add(left)
            }

            val up = grid.getUp(coordinates)
            val upTile = grid.getTile(up)

            if (upTile in getValidNextTile(tile, Direction.UP)) {
                toVisit.add(up)
            }

            val down = grid.getDown(coordinates)
            val downTile = grid.getTile(down)

            if (downTile in getValidNextTile(tile, Direction.DOWN)) {
                toVisit.add(down)
            }
        }
    }

    private fun getValidNextTile(tile: Char, direction: Direction): List<Char> = when (tile) {

        'S' -> validPipeTransition[direction]!!

        '|' -> when (direction) {
            Direction.UP -> validPipeTransition[direction]!!
            Direction.DOWN -> validPipeTransition[direction]!!
            else -> listOf()
        }

        '-' -> when (direction) {
            Direction.LEFT -> validPipeTransition[direction]!!
            Direction.RIGHT -> validPipeTransition[direction]!!
            else -> listOf()
        }

        'L' -> when (direction) {
            Direction.UP -> validPipeTransition[direction]!!
            Direction.RIGHT -> validPipeTransition[direction]!!
            else -> listOf()
        }

        'J' -> when (direction) {
            Direction.UP -> validPipeTransition[direction]!!
            Direction.LEFT -> validPipeTransition[direction]!!
            else -> listOf()
        }

        'F' -> when (direction) {
            Direction.RIGHT -> validPipeTransition[direction]!!
            Direction.DOWN -> validPipeTransition[direction]!!
            else -> listOf()
        }

        '7' -> when (direction) {
            Direction.LEFT -> validPipeTransition[direction]!!
            Direction.DOWN -> validPipeTransition[direction]!!
            else -> listOf()
        }

        else -> listOf()
    }

    private fun displayPipes() {

        grid.displayGrid { coordinates, tile ->
            if (coordinates in pipes) {
                tile
            } else {
                '.'
            }
        }
    }

}


fun main() {
    Day10().solve()
}