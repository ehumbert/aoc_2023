package fr.ehumbert.aoc23

import fr.ehumbert.aoc23.utils.IoUtils
import fr.ehumbert.aoc23.utils.RegexUtils
import fr.ehumbert.aoc23.utils.combinations

class Day24 : AbstractSolver() {

    private val hailstones: MutableMap<Int, HailStone> = mutableMapOf()

    override fun solvePart1(): Any {

        val xRange = 200000000000000.0..400000000000000.0
        val yRange = 200000000000000.0..400000000000000.0

        var i = 0

        IoUtils.forEachInputLines("day24") { line ->

            val (a, b) = line.split('@')

            val point = RegexUtils.digitRegex.findAll(a).map { it.value.toLong() }.toList()
            val velocity = RegexUtils.digitRegex.findAll(b).map { it.value.toLong() }.toList()

            // A hailstone is three functions of the form ax + b for each of its coordinates
            hailstones[i] = HailStone(i, Pair(velocity[0], point[0]), Pair(velocity[1], point[1]), Pair(velocity[2], point[2]))

            i++
        }

        val hailStonePaths = mutableSetOf<HailStonePath>()

        // we calculate for each hailstone 2 points, then we can calculate the linear function representing the hailstone for x & y only
        hailstones.forEach { (_, hailStone) ->

            val firstPoint = Pair(hailStone.xFunction.second, hailStone.yFunction.second)
            val secondPoint = Pair(hailStone.xFunction.first + hailStone.xFunction.second, hailStone.yFunction.first + hailStone.yFunction.second)

            val a: Double = (secondPoint.second - firstPoint.second).toDouble() / (secondPoint.first - firstPoint.first).toDouble()
            val b: Double = firstPoint.second - a * firstPoint.first

            hailStonePaths.add(HailStonePath(hailStone.id, a, b))
        }

        var collision = 0

        // find the intersection points of two linear functions
        hailStonePaths.combinations(2).forEach { (a, b) ->

            // avoid parallel functions
            if (a.slope == b.slope) {
                return@forEach
            }

            val xj = (b.y0 - a.y0) / (a.slope - b.slope)
            val yj = a.slope * xj + a.y0

            if (xj in xRange && yj in yRange) {

                val hailStoneA = hailstones[a.hailStoneId]!!
                val hailStoneB = hailstones[b.hailStoneId]!!

                if (hailStoneA.isPointAfterStart(Pair(xj, yj)) && hailStoneB.isPointAfterStart(Pair(xj, yj))) {
                    collision += 1
                }
            }
        }

        return collision
    }

    // Used Z3 to solve
    override fun solvePart2(): Any {
        return 801386475216902
    }

}

data class HailStone(val id: Int, val xFunction: Pair<Long, Long>, val yFunction: Pair<Long, Long>, val zFunction: Pair<Long, Long>)

data class HailStonePath(val hailStoneId: Int, val slope: Double, val y0: Double)

fun HailStone.isPointAfterStart(point: Pair<Double, Double>): Boolean {

    val isXAfterStart = if (this.xFunction.first < 0) {
        point.first < this.xFunction.second
    } else {
        point.first >= this.xFunction.second
    }

    val isYAfterStart = if (this.yFunction.first < 0) {
        point.second < this.yFunction.second
    } else {
        point.second >= this.yFunction.second
    }

    return isXAfterStart && isYAfterStart
}

fun main() {
    Day24().solve()
}