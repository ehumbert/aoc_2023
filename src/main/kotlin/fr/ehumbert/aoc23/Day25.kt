package fr.ehumbert.aoc23

import fr.ehumbert.aoc23.utils.IoUtils

class Day25 : AbstractSolver() {

    private val connections: MutableMap<String, MutableList<String>> = mutableMapOf()

    /*
     * using graphviz to find the 3 bridges
     * dot -Tsvg /tmp/graphviz.dot > /tmp/day25.svg
     * rph <-> gst
     * sfd <-> ljm
     * jkn <-> cfn
     *
     * Algorithm to solve it programmatically is called min-cut
     * Stoer–Wagner seems interesting : https://en.wikipedia.org/wiki/Stoer%E2%80%93Wagner_algorithm
     */
    override fun solvePart1(): Any {

        val lines = mutableListOf<String>()

        IoUtils.forEachInputLines("day25", false) { line ->

            val (a, b) = line.split(':')
            val c = b.trim().split(' ')

            c.forEach {
                connections.getOrPut(a) { mutableListOf() }.add(it)
                connections.getOrPut(it) { mutableListOf() }.add(a)
                lines.add("    $a -> $it")
            }
        }

        lines.sort()
        lines.addFirst("digraph G {")
        lines.addLast("}")

        IoUtils.writeLines("/tmp/graphviz.dot", lines)

        val bridges = listOf(
            Pair("rph", "gst"),
            Pair("sfd", "ljm"),
            Pair("jkn", "cfn")
        )

        bridges.forEach { (left, right) ->
            connections[left]!!.remove(right)
            connections[right]!!.remove(left)
        }

        val left = getComponentConnections("rph") + 1
        val right = getComponentConnections("gst") + 1

        return "$left x $right = ${left * right}"
    }

    override fun solvePart2(): Any {
        return "no part 2"
    }

    private fun getComponentConnections(component: String, seen: MutableSet<String> = mutableSetOf()): Int {

        if (component in seen) {
            return 0
        }

        seen.add(component)

        var count = 0

        connections[component]!!.forEach {
            if (it !in seen) {
                count += 1 + getComponentConnections(it, seen)
            }
        }

        return count
    }

}

fun main() {
    Day25().solve()
}