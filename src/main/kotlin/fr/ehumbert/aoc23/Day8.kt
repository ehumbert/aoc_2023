package fr.ehumbert.aoc23

import fr.ehumbert.aoc23.utils.IoUtils
import fr.ehumbert.aoc23.utils.MathsUtils

class Day8 : AbstractSolver() {

    private val keyRegex = Regex("[0-9A-Z]{3}")

    private var instructions = ""
    private val desertMap: MutableMap<String, Pair<String, String>> = mutableMapOf()

    override fun solvePart1(): Any {

        parseInput()

        var position = "AAA"
        var steps = 0

        while (position != "ZZZ") {

            instructions.forEach { instruction ->

                position = when (instruction) {
                    'L' -> desertMap[position]?.first ?: throw RuntimeException("Unknown key $position")
                    'R' -> desertMap[position]?.second ?: throw RuntimeException("Unknown key $position")
                    else -> throw RuntimeException("Unknown instruction $instruction")
                }

                steps += 1
            }
        }

        return steps
    }

    override fun solvePart2(): Any {

        val positions: MutableMap<Int, String> = mutableMapOf()
        val nextPositions: MutableMap<Int, String> = mutableMapOf()

        val finishIndex: MutableMap<Int, Int> = mutableMapOf()

        desertMap.keys.filter { it.endsWith('A') }.forEachIndexed { index, s ->
            positions[index] = s
        }

        val startsNumber = positions.size

        var isFinished = false
        var step = 0

        while (!isFinished) {

            instructions.forEach { instruction ->

                step += 1

                positions.forEach positionLoop@{ (i, position) ->

                    val nextPosition = when (instruction) {
                        'L' -> desertMap[position]?.first ?: throw RuntimeException("Unknown key $position")
                        'R' -> desertMap[position]?.second ?: throw RuntimeException("Unknown key $position")
                        else -> throw RuntimeException("Unknown instruction $instruction")
                    }

                    if (nextPosition.endsWith('Z')) {

                        if (i !in finishIndex) {
                            finishIndex[i] = step
                        }

                        if (finishIndex.size == startsNumber) {
                            isFinished = true
                            return@positionLoop
                        }
                    }

                    nextPositions[i] = nextPosition
                }

                if (isFinished) {
                    return@forEach
                }

                positions.clear()
                positions.putAll(nextPositions)
                nextPositions.clear()
            }
        }

        val result = MathsUtils.findLcm(finishIndex.values.map { it.toLong() }.toList())

        return result
    }

    private fun parseInput() {

        var isInstruction = true

        IoUtils.forEachInputLines("day8") parser@{ line ->

            if (line.isBlank()) {
                isInstruction = false
                return@parser
            }

            if (isInstruction) {
                instructions += line.trim()
                return@parser
            }

            val input = keyRegex.findAll(line).map { it.value }.toList()

            desertMap[input[0]] = Pair(input[1], input[2])
        }
    }

}

fun main() {
    Day8().solve()
}