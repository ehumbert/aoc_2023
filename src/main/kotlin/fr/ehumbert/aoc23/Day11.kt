package fr.ehumbert.aoc23

import fr.ehumbert.aoc23.utils.Coordinates
import fr.ehumbert.aoc23.utils.Grid
import fr.ehumbert.aoc23.utils.IoUtils
import fr.ehumbert.aoc23.utils.manhattanDistance
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min

class Day11 : AbstractSolver() {

    override fun solvePart1(): Any {

        val input: MutableList<String> = mutableListOf()

        IoUtils.forEachInputLines("day11") { line ->

            input.add(line)

            if (line.all { it == '.' }) {
                input.add(line)
            }
        }

        val columnsToAdd = mutableListOf<Int>()

        for (y in 0..<input.first().length) {
            if (input.map { it[y] }.all { it == '.' }) {
                columnsToAdd.add(y)
            }
        }

        val space = input.map { line ->

            var expandedLine = line

            columnsToAdd.forEachIndexed { i, y ->
                expandedLine = expandedLine.substring(0, y + i) + '.' + expandedLine.substring(y + i)
            }

            expandedLine
        }

        val grid = Grid(space)
        val galaxies = grid.findAllInGrid('#')

        val distances: MutableMap<Pair<Int, Int>, Long> = mutableMapOf()

        galaxies.forEachIndexed { i, a ->
            galaxies.forEachIndexed second@{ j, b ->

                if (a == b) {
                    return@second
                }

                val key = Pair(min(i, j), max(i, j))

                if (key in distances) {
                    return@second
                }

                // Manhattan distance
                val distance = abs(b.first - a.first) + abs(b.second - a.second)

                distances[key] = distance
            }
        }

        return distances.values.sum()
    }

    override fun solvePart2(): Any {

        val space: MutableList<String> = mutableListOf()

        val emptyLines = mutableListOf<Int>()
        val emptyColumns = mutableListOf<Int>()

        val expansionRate = 1_000_000

        IoUtils.readAllInputLines("day11").forEachIndexed { x, line ->

            space.add(line)

            if (line.all { it == '.' }) {
                emptyLines.add(x)
            }
        }

        for (y in 0..<space.first().length) {
            if (space.map { it[y] }.all { it == '.' }) {
                emptyColumns.add(y)
            }
        }

        val grid = Grid(space)

        val galaxies = grid.findAllInGrid('#').map { (x, y) ->
            Coordinates(
                x + (emptyColumns.count { it < x } * (expansionRate - 1)),
                y + (emptyLines.count { it < y } * (expansionRate - 1))
            )
        }

        val distances: MutableMap<Pair<Int, Int>, Long> = mutableMapOf()

        var sum = 0L

        galaxies.forEachIndexed { i, a ->
            galaxies.forEachIndexed second@{ j, b ->

                if (a == b) {
                    return@second
                }

                val key = Pair(min(i, j), max(i, j))

                if (key in distances) {
                    return@second
                }

                // Manhattan distance
                val distance = a.manhattanDistance(b)

                distances[key] = distance
                sum += distance
            }
        }

        return sum
    }

}

fun main() {
    Day11().solve()
}