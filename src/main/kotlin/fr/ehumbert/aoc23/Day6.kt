package fr.ehumbert.aoc23

import fr.ehumbert.aoc23.utils.IoUtils
import fr.ehumbert.aoc23.utils.RegexUtils

class Day6 : AbstractSolver() {

    override fun solvePart1(): Any {

        val lines = IoUtils.readAllInputLines("day6")

        val raceTimes = RegexUtils.digitRegex.findAll(lines[0]).map { it.value.toInt() }
        val raceDistances = RegexUtils.digitRegex.findAll(lines[1]).map { it.value.toInt() }

        val races = raceTimes.zip(raceDistances)

        var waysToWin = 1

        races.forEach { (raceTime, raceDistance) ->

            var wonRaces = 0

            for (i in 0..raceTime) {

                val distance = i * (raceTime - i)

                if (distance > raceDistance) {
                    wonRaces++
                }
            }

            waysToWin *= wonRaces
        }

        return waysToWin
    }

    override fun solvePart2(): Any {

        val lines = IoUtils.readAllInputLines("day6", false)

        val raceTime = RegexUtils.digitRegex.findAll(lines[0]).map { it.value }.joinToString("").toLong()
        val raceDistance = RegexUtils.digitRegex.findAll(lines[1]).map { it.value }.joinToString("").toLong()

        var wonRaces = 0

        for (i in 0..raceTime) {

            val distance = i * (raceTime - i)

            if (distance > raceDistance) {
                wonRaces++
            }
        }

        return wonRaces
    }

}

fun main() {
    Day6().solve()
}