package fr.ehumbert.aoc23

import fr.ehumbert.aoc23.utils.IoUtils
import kotlin.math.max


class Day2 : AbstractSolver() {

    override fun solvePart1(): Any {

        val possibleGames: MutableList<Int> = mutableListOf()

        val redRegex = Regex("([1][3-9]|[2-9]\\d|\\d{3,}) red")
        val greenRegex = Regex("([1-9][4-9]|[2-9]\\d|\\d{3,}) green")
        val blueRegex = Regex("([1-9][5-9]|[2-9]\\d|\\d{3,}) blue")

        IoUtils.forEachInputLines("day2") { line ->

            var input = line.split(':')

            val game = Regex("\\d+").find(input[0])!!.value.toInt()
            input = input.drop(1)

            input.forEach { drawsInput ->

                val draws = drawsInput.split(';')

                val drawIsInvalid = draws.any {
                    redRegex.containsMatchIn(it) || greenRegex.containsMatchIn(it) || blueRegex.containsMatchIn(it)
                }

                if (!drawIsInvalid) {
                    possibleGames.add(game)
                }
            }
        }

        return possibleGames.sum()
    }

    override fun solvePart2(): Any {

        val gamePowers: MutableList<Int> = mutableListOf()

        val redRegex = Regex("(\\d+) red")
        val greenRegex = Regex("(\\d+) green")
        val blueRegex = Regex("(\\d+) blue")

        IoUtils.forEachInputLines("day2") { line ->

            val input = line.split(':').drop(1)

            var maxRed = 0
            var maxGreen = 0
            var maxBlue = 0

            input.forEach { drawsInput ->

                val draws = drawsInput.split(';')

                draws.forEach {
                    maxRed = max(maxRed, redRegex.find(it)?.groupValues?.get(1)?.toInt() ?: 0)
                    maxGreen = max(maxGreen, greenRegex.find(it)?.groupValues?.get(1)?.toInt() ?: 0)
                    maxBlue = max(maxBlue, blueRegex.find(it)?.groupValues?.get(1)?.toInt() ?: 0)
                }

                gamePowers.add(maxRed * maxGreen * maxBlue)
            }
        }

        return gamePowers.sum()
    }

}

fun main() {
    Day2().solve()
}