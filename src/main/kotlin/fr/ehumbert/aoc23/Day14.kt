package fr.ehumbert.aoc23

import fr.ehumbert.aoc23.utils.Coordinates
import fr.ehumbert.aoc23.utils.Grid
import fr.ehumbert.aoc23.utils.IoUtils

class Day14 : AbstractSolver() {

    private var grid = Grid(IoUtils.readAllInputLines("day14", false))

    override fun solvePart1(): Any {

        var result = 0L

        // Move everything up
        grid.iterateGrid { coordinates, tile ->

            if (tile == 'O') {

                var destination = coordinates

                while (true) {

                    val upCoordinates = grid.getUp(destination)
                    val up = grid.getTile(upCoordinates)

                    if (up == '.') {
                        destination = upCoordinates
                    } else {
                        break
                    }
                }

                if (destination != coordinates) {
                    grid.putTile(destination, 'O')
                    grid.putTile(coordinates, '.')
                }

                result += 1 + grid.maxY - destination.second
            }
        }

        return result
    }

    override fun solvePart2(): Any {

        var result = 0L

        var cycleStart = 0
        var cycleEnd = 0
        val target = 1_000_000_000

        grid = Grid(IoUtils.readAllInputLines("day14"))

        val history: MutableMap<Set<Coordinates>, Int> = mutableMapOf()
        var previous: Set<Coordinates> = setOf()

        for (i in 0..target) {

            when (i % 4) {
                0, 1 -> grid.iterateGrid { coordinates, tile -> moveRock(coordinates, tile, i) }
                2, 3 -> grid.reverseIterateGrid { coordinates, tile -> moveRock(coordinates, tile, i) }
            }

            if (i % 4 == 3) {

                val current = grid.findAllInGrid('O').toSet()

                // cycle
                if (previous.isNotEmpty()) {

                    if (current in history) {
                        cycleStart = history[current]!!
                        cycleEnd = i / 4
                        break
                    }

                    history[current] = i / 4
                }

                previous = current
            }
        }

        val cycle = cycleEnd - cycleStart
        val offset = (target - cycleStart) % cycle

        val finalPositions = history.entries.find { (_, i) -> i == cycleStart + offset - 1 }!!.key

        finalPositions.forEach { (_, y) ->
            result += 1 + grid.maxY - y
        }

        return result
    }

    private fun moveRock(coordinates: Coordinates, tile: Char, i: Int) {

        if (tile == 'O') {

            var destination = coordinates

            while (true) {

                val nextCoordinates = when (i % 4) {
                    0 -> grid.getUp(destination)
                    1 -> grid.getLeft(destination)
                    2 -> grid.getDown(destination)
                    3 -> grid.getRight(destination)
                    else -> throw RuntimeException()
                }

                val next = grid.getTile(nextCoordinates)

                if (next == '.') {
                    destination = nextCoordinates
                } else {
                    break
                }
            }

            if (destination != coordinates) {
                grid.putTile(destination, 'O')
                grid.putTile(coordinates, '.')
            }
        }
    }

}

fun main() {
    Day14().solve()
}